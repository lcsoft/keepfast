function wrapAction(callback) {
  return async (...args) => {
    try {
      await callback(...args)
    } catch (err) {
      console.error('error:', err.message)
      process.exit(-1)
    }
  }
}

module.exports = {
  wrapAction
}
