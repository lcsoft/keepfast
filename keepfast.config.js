module.exports = {
  repository: {
    owner: 'gitee-frontend',
    name: 'keepfast'
  },
  build: false,
  lighthouse: false
}
