#!/usr/bin/env node

const fs = require('fs')
const path = require('path')
const { spawn, exec } = require('child_process')
const psList = require('ps-list')
const pidusage = require('pidusage')
const simpleGit = require('simple-git')
const { program } = require('commander')
const { parseArgsStringToArgv } = require('string-argv')
const { wrapAction } = require('../lib/utils')
const loadConfig = require('../lib/config').load

const cwd = process.cwd()
const maxBuildTime = 15 * 60 * 1000

// 解析构建性能报告
function parseBuildReportFile(file) {
  const report = require(file)
  return {
    version: report.version,
    modules: report.modules.map((m) => ({
      id: m.id,
      size: m.size,
      name: m.name,
      identifier: m.identifier,
      built: m.built,
      cacheable: m.cacheable
    })),
    assets: report.assets.map((a) => ({
      size: a.size,
      name: a.name,
      chunks: a.chunks,
      chunkNames: a.chunkNames
    })),
    chunks: report.chunks.map((c) => ({
      id: c.id,
      names: c.names,
      hash: c.hash,
      files: c.files,
      modules: c.modules.map((m) => m.identifier)
    })),
    entrypoints: report.entrypoints
  }
}

// 获取 vue-cli-service 进程 id
async function getBuildToolPid(config) {
  if (!config.tool) {
    throw new Error(`invalid tool name: ${config.tool}`)
  }
  const getPid = () => new Promise((resolve, reject) => {
    setTimeout(async () => {
      psList().then((list) => {
        const proc = list.find((p) => p.cmd.includes(`.bin/${config.tool}`))

        resolve(proc ? proc.pid : null)
      }).catch(reject)
    }, 500)
  })

  for (let i = 0; i < 10; ++i) {
    // eslint-disable-next-line no-await-in-loop
    const pid = await getPid()
    if (pid) {
      return pid
    }
  }
  throw new Error(`${config.tool} process id was not found`)
}

// 生成构建性能报告
async function generateBuildReport(config) {
  let timer
  let report
  let maxMemoeryUsage = 0
  const startTime = Date.now()
  const file = path.resolve(cwd, config.outputPath, config.reportPath)

  const runScript = () => new Promise((resolve, reject) => {
    exec(config.script, (err) => {
      if (err) {
        reject(err)
        return
      }
      resolve()
    })
    getBuildToolPid(config).then((pid) => {
      console.log(`the pid of the build process: ${pid}`)
      timer = setInterval(() => {
        if (Date.now() - startTime > maxBuildTime) {
          process.kill(pid, 'SIGINT')
          reject(new Error(`build time exceeded limit (${maxBuildTime / 1000} s)`))
          return
        }
        pidusage(pid, (err, stats) => {
          if (err) {
            console.error(err)
            return
          }
          maxMemoeryUsage = Math.max(stats.memory, maxMemoeryUsage)
        })
      }, 1000)
    })
  })

  try {
    await runScript()
    report = parseBuildReportFile(file)
  } catch (err) {
    report = {
      version: 'unknown',
      error: true,
      message: err.message,
      modules: [],
      assets: [],
      chunks: [],
      entrypoints: []
    }
  }
  report.memory = maxMemoeryUsage
  clearInterval(timer)
  return report
}

// 生成 lighthouse 性能报告
async function generateLighthouseReport(config) {
  const configPath = path.join(cwd, 'lighthouse.config.js')
  const reportPath = path.join(cwd, 'lighthouse.json')
  const address = await new Promise((resolve, reject) => {
    const [cmd, ...args] = parseArgsStringToArgv(config.script)
    console.log('starting server for lighthouse')
    spawn(cmd, args)
      .on('exit', resolve)
      .on('error', reject)
      .stdout.on('data', (data) => {
        const key = 'Local:'
        const str = data.toString().split('\n').find((line) => line.includes(key))
        if (str) {
          resolve(str.substr(str.indexOf(key) + key.length + 1).trim())
        }
      })
  })

  await new Promise((resolve, reject) => {
    const url = `${address}${config.pagePath}`
    console.log(`running lighthouse to access ${url}`)
    spawn(
      'lighthouse',
      [
        url,
        fs.existsSync(configPath) ? `--config-path=${configPath}` : '',
        '--chrome-flags="--headless --no-sandbox"',
        `--locale=${config.locale}`,
        '--output=json',
        `--output-path=${reportPath}`
      ],
      {
        stdio: 'inherit'
      }
    )
      .on('exit', resolve)
      .on('error', reject)
  })
  process.kill(await getBuildToolPid(config))
  return require(reportPath)
}

// 生成构建性能差异报告
async function generateBuildDiffReport(config) {
  let report
  const git = simpleGit(cwd)
  const stableBranchHash = (await git.revparse(config.stableBranch)).trim()
  const currentHash = (await git.revparse('HEAD')).trim()
  const currentBranch = (await git.revparse(['--abbrev-ref', 'HEAD'])).trim()
  const finalReport = { time: new Date(), version: '', results: [] }

  async function build(branch, commit) {
    const startTime = new Date()
    await git.checkout(branch)
    console.log(`starting build for branch: ${branch}`)
    report = await generateBuildReport(config)
    report.time = new Date() - startTime
    report.branch = branch
    report.commit = commit
    if (report.message) {
      console.log(`${branch} branch build failed, it takes ${report.time / 1000} s`)
    } else {
      console.log(`${branch} branch build completed, it takes ${report.time / 1000} s`)
    }
    return report
  }

  report = await build(config.stableBranch, stableBranchHash)
  finalReport.version = report.version
  finalReport.results.push(report)
  if (stableBranchHash !== currentHash) {
    report = await build(currentBranch, currentHash)
    finalReport.results.push(report)
  }
  await git.checkout(currentBranch)
  return finalReport
}

async function generateReport(config, output) {
  const report = {}

  if (config.build) {
    report.build = await generateBuildDiffReport(config.build)
  }
  if (config.lighthouse) {
    report.lighthouse = await generateLighthouseReport(config.lighthouse)
  }
  if (!output) {
    console.log(JSON.stringify(report, null, 2))
    return
  }
  if (!fs.existsSync(path.dirname(output))) {
    fs.mkdir(path.dirname(output), { recursive: true })
  }
  fs.writeFileSync(output, JSON.stringify(report, null, 2))
}

program
  .arguments('[reportFile]')
  .action(wrapAction(async (output) => {
    const config = loadConfig()
    await generateReport(config, output)
  }))
  .parse(process.argv)
