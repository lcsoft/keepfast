#!/usr/bin/env node

const fs = require('fs')
const { program } = require('commander')
const { defaults, filename } = require('../lib/config')
const { wrapAction } = require('../lib/utils')

program
  .action(wrapAction(() => {
    fs.writeFileSync(filename, `module.exports = ${JSON.stringify(defaults, null, 2)}\n`)
  }))
  .parse(process.argv)
