#!/usr/bin/env node

const path = require('path')
const { program } = require('commander')

program
  .command('init', 'initialize a default configuration file')
  .command('report [reportFile]', 'run performance tests and generate report')
  .command('upload [options] [reportFile]', 'upload report')

program
  .version(require(path.join(__dirname, '..', 'package.json')).version)
  .parse(process.argv)
