// 用于在开发环境下测试网站性能的配置
module.exports = {
  extends: 'lighthouse:default',
  settings: {
    emulatedFormFactor: 'desktop',
    throttlingMethod: 'provided',
    skipAudits: [
      'uses-http2',
      'uses-passive-event-listeners',
      'uses-long-cache-ttl'
    ],
    onlyCategories: [
      'performance'
    ]
  }
}
